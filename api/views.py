from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .serializers import WeatherSerializer

from api.weather import WeatherAPI


@api_view(['GET'])
def weather_temperatures(request, city):
    if request.method == 'GET':
        try:
            days = request.GET["days"]
        except KeyError:
            return Response(
                'Please Include days as query parameter',
                status=status.HTTP_406_NOT_ACCEPTABLE
                 )

        # check that days are in number format
        try:
            int(days)
        except ValueError:
            return Response(
                'Days should be an integer',
                status=status.HTTP_406_NOT_ACCEPTABLE
                )

        weather_api = WeatherAPI()
        weather_report = weather_api.city_weather_forecast(city, days)
        status_code = weather_report[0]

        if status_code != 200:
            return Response(weather_report[1], status=status_code)
        else:
            weather_report_data = weather_report[1]
            serializer = WeatherSerializer(weather_report_data)
            # add custom partial content since api only
            # supports upto 14 days of weather forecast
            if int(days) > 14:
                return Response(
                    serializer.data,
                    status=status.HTTP_206_PARTIAL_CONTENT
                    )
            else:
                return Response(serializer.data)
