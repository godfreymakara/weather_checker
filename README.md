
# Weather temperature 
An api for providing weather forecast for a city
for a given days


## API Reference

#### Get weather items for a spefic city for number of days

```http
  GET /api/locations/{city}/?days={number_of_days}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `city` | `string` | **Required**. City Name |
| `days` | `int` | **Required**. Days |



## Badges




## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`DEBUG`

`SECRET_KEY`

`ALLOWED_HOSTS`

To access the weather API,Add the WEATHER_API_KEY below
`WEATHER_API_KEY= df882f0b551a4463a8064542221809`


 


## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:godfreymakara/weather_checker.git
```

Go to the project directory

```bash
  cd weather_checker
```

Create virtual environment and activate

```bash
  python3 -m venv venv
  source venv/bin/activate
```
Create .env file and add environment variables indicated above

```bash
  touch .env
```

Install Depedencies

```bash
  pip install -r requirements.txt
```
Run Server

```bash
  python manage.py migrate
  python manage.py runserver
```

## Running Tests

To run tests  run the following command

```bash
  tox
```


## Appendix

Any additional information goes here


## Tech Stack

**Server:** Django, Django_rest_framework


## API return

```json
{
“maximum”: value,
“minimum”: value,
“average”: value,
“median”: value,
}
```

